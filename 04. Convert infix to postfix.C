#include<stdio.h>
#include<conio.h>

//Including ctype.h to use the function "isalpha" & "isdigit"
#include<ctype.h>

//Declare a Stack to store notations
char stk[10],val;

//Initialize top with -1
int top=-1;

//Function Declarations
void push(char c);
char pop(char st[]);
int priority(char ch);
void convert(char infix[],char postfix[]);

//Main Execution
void main()
{
	char in[20],post[20];
	clrscr();
	printf("\nEnter the infix expression : ");
	gets(in);
	convert(in,post);
	getch();
}

//Define "CONVERT" that makes infix notation to postfix
void convert(char infix[20],char postfix[20])
{
	int i,j=0;
	char ch;

	//Loop will execute until it encounters a NULL character
	for(i=0;infix[i]!='\0';i++)
	{
		ch=infix[i];
		//Checks if character is "(" than pushes to stack
		if(ch=='(')
		{
			push(ch);
		}
		//If not than checks rather it is alphabet or digit
		else if(isalpha(ch)==1 || isdigit(ch)==1)
		{
			postfix[j]=ch;
			j++;
		}
		//If mathematical expressions encounter than it checks its priority
		else if(ch=='*' || ch=='%' || ch=='/' || ch=='+' || ch=='-')
		{
			if(priority(ch)==0)
			{

				while(priority(stk[top])>priority(ch))
				{
					postfix[j]=pop(stk[top]);
					j++;
				}
				push(ch);
			}
			else
			{
				push(ch);
			}
		}
		//If ")" encounters, it pops the notations from stack until it encounters "(" in stack
		else if(ch==')')
		{
			while(stk[top]!='(')
			{
				postfix[j]=pop(stk[top]);
				j++;
			}top--;
		}
		//If nothing matches than its invalid expression :P
		else
		{
			printf("\nInvalid Expression.");
		}
	}

	//After everything it prints postfixed expression
	while(top>=0)
	{
		postfix[j]=pop(stk[top]);
		j++;
	}
	postfix[j]='\0';
	printf("\nPostfix Expression :");
	puts(postfix);
}

//Define a function that checks the priotity of Mathematical expressions
int priority(char ch)
{
	//Using simple else if ladder we have defined the priotity
	if(ch=='%' || ch=='*' || ch=='/')
		return 1;
	else if(ch=='+' || ch=='-')
		return 0;
}

//Define the function "PUSH" that pushes the mathematical notations to the stack
void push(char c)
{
	top++;
	stk[top]=c;
}

//Define the function "POP" that deletes the mathematical notations from the stack
char pop(char st[])
{
	val=st[top];
	top--;
	return val;
}
