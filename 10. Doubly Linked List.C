#include<stdio.h>
#include<conio.h>

//"Define the Self Reffering Structure Pointer after data part, as declaring before data part will causes bug in program" Main mistake that prevents output
struct node
{
	int data;
	struct node *next,*prev;
};

//Define struct node as a link datatype
typedef struct node link;

// Initialize a starting pointer
link *start=NULL;

//Insert Node at Begining
void insert_beg()
{
	link *n1;
	int num;
	printf("Enter the number you want to enter:");
	scanf("%d",&num);
	n1=(link *)malloc(sizeof (link *));
	n1->data=num;
	if(start==NULL)
	{
		start=n1;
		n1->next=NULL;
		n1->prev=NULL;
	}
	else
	{
		n1->next=start;
		n1->prev=NULL;
		start->prev=n1;
		start=n1;
	}
}

//Insert Node at Last
void insert_end()
{
	link *n1,*ptr;
	int num;
	printf("Enter the number:");
	scanf("%d",&num);
	n1=(link *)malloc(sizeof (link *));
	n1->data=num;
	if(start==NULL)
	{
		start=n1;
		n1->next=NULL;
		n1->prev=NULL;
	}
	else
	{
		ptr=start;
		while(ptr->next != NULL)
		{
			ptr=ptr->next;
		}

		ptr->next=n1;
		n1->next=NULL;
		n1->prev=ptr;

	}
}

//Display Elements of the Link
void display()
{
	link *ptr=start;
	if(ptr==NULL)
	{
		printf("Linklists empty\n\n");
	}
	else
	{
		while(ptr != NULL)
		{
			printf(" %d -> ",ptr->data);
			ptr=ptr->next;
		}
	}
}

//Delete Node from the begining
void delete_beg()
{
	link *n1;
	n1=start;
	if (start==NULL)
	{
		printf("Link List is Empty\n\n");
	}
	else
	{
		start=n1->next;
		free(n1);
		printf("Element Deleted Succesfully");
	}
}
//Delete Last
void delete_end()
{
	link *ptr;
	if(start==NULL)
	{
		printf("Link List is Empty");
	}
	else
	{
		while(ptr->next != NULL)
		{
		      ptr=ptr->next;
		}
		ptr->prev->next=NULL;
		free(ptr);
	}
}

//Delet before a specific node
void delete_before()
{
	link *ptr;
	int val;
	if(start==NULL)
	{
		printf("Linked List is Empty\n\n");
	}
	else
	{
		printf("From which Value you want to delete before?:");
		scanf("%d",&val);
		ptr=start;
		while(ptr->next->data != val)
		{
			ptr=ptr->next;
		}
		ptr->prev->next=ptr->next;
		ptr->next->prev=ptr->prev;
		free(ptr);
	}
}

//Delete After Element
void delete_after()
{
	link *ptr;
	int val;
	ptr=start;
	if(start==NULL)
	{
		printf("Linked List is empty");
	}
	else
	{
		printf("Enter the data of node:");
		scanf("%d",&val);
		while(ptr->data != val)
		{
			ptr=ptr->next;
		}
		ptr->next->next->prev=ptr;
		ptr->next=ptr->next->next;
		free(ptr);
		printf("Node Deleted");
	}
}

//Sort the Linked List
void sort()
{
	link *ptr,*ptr1;
	int temp;
	ptr=start;
	while(ptr!=NULL)
	{
		ptr1=ptr->next;
		while(ptr1!=NULL)
		{
			if(ptr1->data < ptr->data)
			{
				temp=ptr->data;
				ptr->data=ptr1->data;
				ptr1->data=temp;
			}
			ptr1=ptr1->next;
		}
		ptr=ptr->next;
	}
}

//Insert Element in sorted linked list
void insert_sorted()
{
	link *ptr,*new_node;
	int n;
	sort();
	new_node=(link *)malloc(sizeof ( link *));
	printf("Enter The value you want to add in the sorted link list:");
	scanf("%d",&n);
	new_node->data=n;
	while(ptr->next->data<n)
	{
		ptr=ptr->next;
	}
	ptr->next=new_node;
	new_node->prev=ptr;
	new_node->next=ptr->next;
	ptr->next->prev=new_node;
}


void main()
{
	int choice;
	clrscr();
	while(1)
	{
		printf("\n\n");
		printf("<-----Doubly Linked List----->");
		printf("\n1. Insert at beginig");
		printf("\n2. Insert at End");
		printf("\n3. Display Linked List");
		printf("\n4. Delete at Begining");
		printf("\n5. Delete at End");
		printf("\n6. Delete Before");
		printf("\n7. Delete After");
		printf("\n8. Insert into sorted linked list");
		printf("\n9. Sort the linked list");
		printf("\n Enter 0 to exit");
		printf("\n<---------->");
		printf("\nEnter Your Choice:");
		scanf("%d",&choice);
		switch(choice)
		{
			case 1:	insert_beg();
				break;
			case 2:	insert_end();
				break;
			case 3:	display();
				break;
			case 4:	delete_beg();
				break;
			case 5: delete_end();
				break;
			case 6:	delete_before();
				break;
			case 7: delete_after();
				break;
			case 8: insert_sorted();
				break;
			case 9: sort();
				break;
			case 0: exit(1);
			default:	printf("Enter Valid Choice");
					                   
		}
	}
}
