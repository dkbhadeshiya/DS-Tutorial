#include<stdio.h>
#include<conio.h>

//Main Execution
void main()
{
	int *ptr,n;
	clrscr();
	printf("Memory Allocation using malloc():");
	
	//Getting the Number of memory blocks to allocate	
	printf("\n\n Enter no of Elements:");
	scanf("%d",&n);
	
	//Allocation of Memory using malloc
	ptr=(int *) calloc (n * sizeof(int));
	
	
	//Check if Memory is allocated or not
	if(ptr==NULL)
	{
		printf("Memory Allocation not done properly");
		exit(1);
	}
	else
	{
		printf("Memory Allocation done succesfully");
	}
	

	//Input the Elements
	for(i=0;i<n;i++)
	{
		printf("Enter Element %d:",i+1);
		scanf("%d",(ptr+i));
	}
	

	//Print elements
	for(i=0;i<n;i++)
	{
		printf("\n Element %d: %d",(i+1),(ptr+i));
	}
	getch();
}
