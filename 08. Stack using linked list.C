#include<stdio.h>
#include<conio.h>
//declare a structure
struct node
{
	int data;
	struct node *next;
};

//Define struct node as a stack datatype
typedef struct node stack;

// Initialize a starting pointer
stack *top=NULL;

//kind of insert_beg
void push()
{
	stack *n1;
	int num;
	//Gets the Data
	printf("Enter the number you want to enter:");
	scanf("%d",&num);
	//Allocates the memory for new node
	n1=(stack *)malloc(sizeof (stack *));
	//Stores the data to element part
	n1->data=num;
	
	if(top==NULL)
	{
		top=n1;
		n1->next=NULL;
	}
	else
	{
		n1->next=top;
		top=n1;
	}
}


//kind of delete from begining
void pop()
{
	stack *n1;
	n1=top;
	//Check if Stack is empty
	if (top==NULL)
	{
		printf("Stack is Empty\n\n");
	}
	else
	{
		top=n1->next;
		free(n1);
		printf("Element Deleted Succesfully");
	}
}

//Display the Stack
void display()
{
	stack *ptr=top;
	//Check if stack is empty
	if(ptr==NULL)
	{
		printf("Stack empty\n\n");
	}
	else
	{
		while(ptr != NULL)
		{
			printf(" %d | ",ptr->data);
			ptr=ptr->next;
		}
	}
}

//Peep Function
void peep()
{
	stack *ptr=top;
	//Check if stack is empty
	if(ptr==NULL)
	{
		printf("Stack is Empty");
	}
	else
	{
		printf("Topmost element is: %d",ptr->data);
	}
}

//Main execution
void main()
{
	int choice;
	clrscr();
	while(1)
	{
		printf("\n\n");
		printf("<-----Stack----->");
		printf("\n1. Push");
		printf("\n2. Pop");
		printf("\n3. Display");
		printf("\n4. Peep");
		printf("\n Enter 0 to exit");
		printf("\n<---------->");
		printf("\nEnter Your Choice:");
		scanf("%d",&choice);
		switch(choice)
		{
			case 1:	push();
				break;
			case 2:	pop();
				break;
			case 3:	display();
				break;
			case 4: peep();
				break;
			case 0: exit(1);
			default:	printf("Enter Valid Choice");
					break;
		}
	}
}
